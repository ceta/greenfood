package z.it.greenfood.utils;

import android.content.res.Resources;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import z.it.greenfood.R;
import z.it.greenfood.app.App;
import z.it.greenfood.domain.Meal;

/**
 * Created by cecibloom on 15/09/2018.
 */

public class DataHandler {

    private static String loadJSONFromAsset(Resources res) {
        String json = null;
        try {
            //InputStream is = App.getContext().getResources().openRawResource(R.raw.meals);
            InputStream is = res.openRawResource(R.raw.meals);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }

            json = writer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static Meal getMealById(Resources res, int id){
        Meal meal = null;
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(res));
            JSONArray lstMeals = obj.getJSONArray("meals");

            for (int i = 0; i < lstMeals.length(); i++) {
                JSONObject object = lstMeals.getJSONObject(i);
                Log.d("Meal", object.getString("name"));

                if (object.getInt("id") == id) {
                    meal = new Meal();
                    meal.setId(object.getInt("id"));
                    meal.setName(object.getString("name"));
                    meal.setQuote(object.getString("quote"));
                    meal.setAlternatives(object.getString("alternatives"));
                    meal.setCo2((float)object.getDouble("co2"));
                    meal.setWater((float)object.getDouble("water"));
                    meal.setEnergy((float)object.getDouble("energy"));
                    meal.setDrawable(object.getString("drawable"));
                    meal.setDescription(object.getString("description"));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return meal;
    }

    public static Meal getMealByBarCode(String barcode){
        Meal meal = new Meal();

        if (barcode.equals("4029764001883")) {
            return new Meal(100, "Club-Mate Cola", 0.14f, 0f, 0.15f, "Tap water", ".... just drink tap water, it produces 800 times less carbon footprint.", "soda", "Lots of sugar and stains your teeth ...");
        } else if (barcode.equals("7610097185554")) {
            return new Meal(100, "Michel Pure Taste", 0.45f, 0f, 0.22f, "Tap water", ".... I have 2 words for you T·A·P   W·A·T·E·R", "orangejuice", "Orange gonna eat your veggies today? If you want a fresh start ....");
        } else if (barcode.equals("7640119220208")) {
            return new Meal(100, "Maya PopCorn", 0.45f, 0f, 0.22f, "Homemade popcorn.", "Green and Healthy!", "popcorn", "Vitamin K at its fullest.");
        } else {
            return null;
        }
    }

    public static Meal getMealByName(Resources res, String name){
        Meal meal = null;
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(res));
            JSONArray lstMeals = obj.getJSONArray("meals");

            for (int i = 0; i < lstMeals.length(); i++) {
                JSONObject object = lstMeals.getJSONObject(i);
                Log.d("Details-->", object.getString("name"));

                if (object.getString("name").toLowerCase().equals(name.toLowerCase())) {
                    meal = new Meal();
                    meal.setId(object.getInt("id"));
                    meal.setName(object.getString("name"));
                    meal.setQuote(object.getString("quote"));
                    meal.setAlternatives(object.getString("alternatives"));
                    meal.setCo2((float)object.getDouble("co2"));
                    meal.setWater((float)object.getDouble("water"));
                    meal.setEnergy((float)object.getDouble("energy"));
                    meal.setDrawable(object.getString("drawable"));
                    meal.setDescription(object.getString("description"));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return meal;
    }

}
