package z.it.greenfood.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by cecibloom on 15/09/2018.
 */

public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}