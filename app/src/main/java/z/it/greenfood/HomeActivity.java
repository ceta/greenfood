package z.it.greenfood;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import z.it.greenfood.domain.Meal;
import z.it.greenfood.tensorFlow.ClassifierActivity;
import z.it.greenfood.utils.DataHandler;

public class HomeActivity extends FlipperActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setFlipper((ViewFlipper) findViewById(R.id.viewFlipperFood));
        setLayouts(14);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.hide();
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveToLeft() {
        super.moveToLeft();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveToRight() {
        super.moveToRight();
    }

    public void back(View view) {
        LinearLayout container = (LinearLayout) view.getParent();
        ImageView food = (ImageView) view;
        TextView name = (TextView) container.getChildAt(1);
        TextView description = (TextView) container.getChildAt(2);

        RelativeLayout front_layout = findViewById(R.id.front);
        front_layout.setVisibility(View.INVISIBLE);
        front_layout.setEnabled(false);

        LinearLayout back_layout = findViewById(R.id.back);
        back_layout.bringToFront();
        back_layout.setVisibility(View.VISIBLE);
        back_layout.setEnabled(true);

        ImageView imageView = findViewById(R.id.image);
        imageView.setImageDrawable(food.getDrawable());

        TextView nameView = findViewById(R.id.txt_image_name);
        nameView.setText(name.getText());

        TextView descriptionView = findViewById(R.id.txt_image_desc);
        descriptionView.setText(description.getText());

        Meal meal = DataHandler.getMealById(getResources(), Integer.valueOf(String.valueOf(food.getTag())));
        createGraph(meal);
    }

    public void front(View view) {
        LinearLayout back_layout = findViewById(R.id.back);
        back_layout.setVisibility(View.INVISIBLE);
        back_layout.setEnabled(false);

        RelativeLayout front_layout = findViewById(R.id.front);
        front_layout.bringToFront();
        front_layout.setVisibility(View.VISIBLE);
        front_layout.setEnabled(true);
    }

    public void createGraph(Meal meal) {
        GraphView graph =  findViewById(R.id.graph);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(1, meal.getCo2()),
                new DataPoint(2, meal.getEnergy()),
                new DataPoint(3, meal.getWater())
        });
        graph.removeAllSeries();
        graph.addSeries(series);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(new String[] {"CO2\n" + meal.getCo2(), "Energy\n" + meal.getEnergy(), "Water\n" + meal.getWater()});
        staticLabelsFormatter.setVerticalLabels(new String[] {"", "", ""});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb( 0, 150, 0);
            }
        });
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.WHITE);
    }

    private static final int RC_BARCODE_CAPTURE = 9001;

    public void scan(View view) {
        Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
    }

    public void showDialog(View view) {
        int id = Integer.valueOf(String.valueOf(view.getTag()));
        Intent intent = new Intent(getApplicationContext(), DialogActivity.class);
        intent.putExtra("meal", DataHandler.getMealById(getResources(), id));
        startActivity(intent);
    }

    public void identify(View view) {
        Intent intent = new Intent(getApplicationContext(), ClassifierActivity.class);
        startActivity(intent);
    }

    public void search(View view) {
        EditText searchView = findViewById(R.id.txt_search);

        if ( searchView.getVisibility() == View.VISIBLE ) {
            String text = searchView.getText().toString();
            if ( text != null ) {
                Meal meal = DataHandler.getMealByName(getResources(),text);
                startActivity(meal);
            } else {
                searchView.setVisibility(View.INVISIBLE);
            }
        } else {
            searchView.setVisibility(View.VISIBLE);
        }
    }

    private void startActivity(Meal meal) {
        Intent intent = new Intent(getApplicationContext(), DefaultActivity.class);
        intent.putExtra("meal", meal);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Meal meal  = DataHandler.getMealByBarCode(barcode.displayValue);
                    startActivity(meal);

                } else {

                }
            } else {

            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}