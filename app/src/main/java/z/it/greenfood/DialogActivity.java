package z.it.greenfood;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import z.it.greenfood.domain.Meal;

/**
 * This class holds the logic to support the action call when a player
 * is trying to leave the play activity while playing.<br /><br />
 *
 * @author tatibloom
 * @version 1.0
 * @since 28/12/2015.
 */
public class DialogActivity extends AppCompatActivity {

    private static final String TAG = DialogActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.hide();
        }

        Meal meal = (Meal) getIntent().getSerializableExtra("meal");

        if ( meal != null ) {
            TextView quotes = findViewById(R.id.txt_dialog_text2);
            quotes.setText(meal.getQuote());

            TextView alternatives = findViewById(R.id.txt_dialog_text4);
            alternatives.setText(meal.getAlternatives());
        }

        findViewById(R.id.ll_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}