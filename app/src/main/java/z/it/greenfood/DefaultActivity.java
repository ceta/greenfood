package z.it.greenfood;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import z.it.greenfood.domain.Meal;
import z.it.greenfood.utils.DataHandler;

public class DefaultActivity extends AppCompatActivity {

    private Meal meal;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        meal = (Meal) getIntent().getSerializableExtra("meal");

        ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null ) {
            actionBar.hide();
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        configureMeal();
    }

    private void configureMeal() {
        if (meal == null || meal.getName() == "") {
            return;
        }

        TextView name = findViewById(R.id.def_front_image_name);
        name.setText(meal.getName());
        TextView description = findViewById(R.id.def_front_image_desc);
        description.setText(meal.getDescription());
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DialogActivity.class);
                intent.putExtra("meal", DataHandler.getMealByName(getResources(), meal.getName()));
                startActivity(intent);
            }
        });

        ImageView image = findViewById(R.id.def_front_image);
        Resources resources = getBaseContext().getResources();
        final int resourceId = resources.getIdentifier(meal.getDrawable(), "drawable",
                getBaseContext().getPackageName());
        if (resourceId > 0)  {
            image.setImageDrawable(resources.getDrawable(resourceId));
        }
    }

    public void back(View view) {
        ImageView food = (ImageView) view;

        TextView name = findViewById(R.id.def_front_image_name);
        TextView description = findViewById(R.id.def_front_image_desc);

        LinearLayout front_layout = findViewById(R.id.def_front);
        front_layout.setVisibility(View.INVISIBLE);
        front_layout.setEnabled(false);

        LinearLayout back_layout = findViewById(R.id.def_back);
        back_layout.bringToFront();
        back_layout.setVisibility(View.VISIBLE);
        back_layout.setEnabled(true);

        ImageView imageView = findViewById(R.id.def_back_image);
        imageView.setImageDrawable(food.getDrawable());

        TextView nameView = findViewById(R.id.def_back_image_name);
        nameView.setText(name.getText());

        TextView descriptionView = findViewById(R.id.def_back_image_desc);
        descriptionView.setText(description.getText());

        createGraph();
    }

    public void front(View view) {
        if (meal == null ) {
            return;
        }

        LinearLayout back_layout = findViewById(R.id.def_back);
        back_layout.setVisibility(View.INVISIBLE);
        back_layout.setEnabled(false);

        LinearLayout front_layout = findViewById(R.id.def_front);
        front_layout.bringToFront();
        front_layout.setVisibility(View.VISIBLE);
        front_layout.setEnabled(true);
    }

    public void createGraph() {
        if ( meal == null ) {
            return;
        }
        GraphView graph = findViewById(R.id.def_back_graph);
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(1, meal.getCo2()),
                new DataPoint(2, meal.getEnergy()),
                new DataPoint(3, meal.getWater())
        });
        graph.removeAllSeries();

        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb( 0, 150, 0);
            }
        });
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.WHITE);
        graph.addSeries(series);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.VERTICAL);
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getGridLabelRenderer().setHumanRounding(true);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(new String[] {"CO2", "Energy", "Water"});
        staticLabelsFormatter.setVerticalLabels(new String[] {"", "", ""});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
    }

}
